# Bachelors thesis

Source code for bachelors thesis

- Use `conda info --envs` to list all environments.
- Use `conda list` to list all packages in the environment
- Use `activate bachelors-thesis` to switch to environment with packages for this project
- Use `conda env export > environment.yml` to export environment

## Run Jupyter notebooks

1. Activate virtual environment `activate bachelors-thesis`
2. Start notebook `jupyter notebook`

For usage in PyCharm token is needed, e.g. `http://localhost:8888/?token=b146766e916d45545f0fd6654e7465c1c08721c871431a17`.
Adress with token is present in command line info while executing `jupyter notebook` 
For this case, running Jupyter without opening browser might be helpfull.

```sh
jupyter notebook --no-browser
```

!! To access `app` as a package `python setup.py install` must be executed before running notebooks

## LIME - Local Interpretable Model-agnostic Explanations

Project is based on [LIME](https://github.com/marcotcr/lime).

## Malicious vs benign URLs

## Datasets

Původní odkaz: [Awesome ML for cybersecurity][02]

V dokumentu budou potřeba citace na tyto datasety.

1. [url_svmlight.tar.gz][01] - anonymizovano
2. dataset /data/data.csv - 400.000 zaznamu, [odkaz][03] na originalni clanek, [odkaz][04] na github, kod pouzit v /app/app.py, clanek jsem i vyfotil a dal do slozky **docs/urls_fsecurity**

Finální dataset je sestavený z dat z [Kaggle](https://www.kaggle.com/antonyj453/urldataset) nakombinovaný s Unified hosts + gambling + fakenews (bad urls).

## Naming convention in data/partials

This folder is used for my artificially made files with url adresses in certain ratio bad:good.

Why bad:good and not good:bad? Alphabet.

data_badpercentage_goodpercentage.csv

## Project folders

### tools

#### ipynb_output_filter.py

Script for stripping output from ipython/jupyter notebooks.
For more information see original [project](https://github.com/toobaz/ipynb_output_filter) on GitHub.

For usage on other computer, several GIT commands have to be executed.
**Warning**: this will enable the feature for all repositories on your computer.
For information how to disable it see project docs.

```sh
git config --global core.attributesfile ~/.gitattributes
git config --global filter.dropoutput_ipynb.clean ~/tools/ipynb_output_filter.py
git config --global filter.dropoutput_ipynb.smudge cat
```

#### compress.bat

This batch file uses `ghostscript` to produce compressed PDF file from `out/main.pdf` to `bachelors_thesis.pdf`.

For use on your computer, you need to provide `%PATH%` to your `ghostscript` installation. 

```batch
SET path=C:\"Program Files"\gs\gs9.26\bin\;%path%
gswin64c -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=../bachelors_thesis.pdf ../out/main.pdf
pause
```

## Results

### Compare datasets results

- 'process'  37100.90 ms
Dataset data_10_90.csv has score 0.9555.

- 'process'  43402.13 ms
Dataset data_20_80.csv has score 0.9525.

- 'process'  45980.33 ms
Dataset data_30_70.csv has score 0.9567.

- 'process'  48113.08 ms
Dataset data_40_60.csv has score 0.9494.

- 'process'  52622.20 ms
Dataset data_50_50.csv has score 0.94955.

- 'process'  55015.38 ms
Dataset data_60_40.csv has score 0.94825.

- 'process'  53999.77 ms
Dataset data_70_30.csv has score 0.9599.

- 'process'  54742.60 ms
Dataset data_80_20.csv has score 0.9796.

- 'process'  59299.40 ms
Dataset data_90_10.csv has score 0.99535.

## Read recommendations

No wonder I am only reading, not being able to write anything. (Source [MIT](http://people.csail.mit.edu/beenkim/papers/BeenK_FinaleDV_ICML2017_tutorial.pdf))

![alt text](./docs/papers_interpretable_ml.png "The amount of research on interpretable machine learning is growing rapidly")

### Articles

1. [Detecting Signs of Ransomware: WannaCry and the Elastic Stack](https://www.elastic.co/blog/malware-analysis-wannacry-elastic-stack)
2. [(Please) Stop Using Unsafe Characters in URLs](https://perishablepress.com/stop-using-unsafe-characters-in-urls/)
3. [Malware increasingly uses DNS as command and control channel to avoid detection, experts say](https://www.pcworld.idg.com.au/article/417011/malware_increasingly_uses_dns_command_control_channel_avoid_detection_experts_say/)
4. [Hacking Scikit-Learn’s Vectorizers](https://towardsdatascience.com/hacking-scikit-learns-vectorizers-9ef26a7170af)
5. (used) [Interpreting machine learning models](https://towardsdatascience.com/interpretability-in-machine-learning-70c30694a05f)
6. [Machine Learning: the importance of explaining your model](https://towardsdatascience.com/machine-learning-the-importance-of-explaining-your-model-29261677b0f4)
7. [How to Identify a Malicious Website](https://techguylabs.com/blog/how-identify-malicious-website)
8. [Trust in LIME: Yes, No, Maybe So?](https://blog.dominodatalab.com/trust-in-lime-local-interpretable-model-agnostic-explanations/)

### SciKit learn documentation

1. [Pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html)
2. [SGDClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html)

### Papers

#### arXiv

1. [Malicious URL Detection using Machine Learning](https://arxiv.org/pdf/1701.07179.pdf)
2. [Manipulating Machine Learning: Poisoning Attacks and Countermeasures for Regression Learning](https://arxiv.org/pdf/1804.00308.pdf)
3. [Deep Neural Network Based Malware Detection Using Two Dimensional Binary Program Features](https://arxiv.org/pdf/1508.03096.pdf)

#### Others

1. [A Feature-rich Machine Learning Framework for Detecting Phishing Web Sites](https://www.ml.cmu.edu/research/dap-papers/dap-guang-xiang.pdf)
2. [Advanced Analytics with Artificial Intelligence: Machine learning combats advanced attacks](https://www.nttsecurity.com/docs/librariesprovider3/resources/global_technical_insight_advanced_analytics_artificial_intelligence_uea_v4)
3. [Security Analytics for Enterprise Threat Detection](http://www.ccs.neu.edu/home/alina/papers/MADE.pdf)
---
[01]: http://www.sysnet.ucsd.edu/projects/url/
[02]: https://github.com/jivoi/awesome-ml-for-cybersecurity
[03]: http://web.archive.org/web/20170514093208/http://fsecurify.com/using-machine-learning-detect-malicious-urls/
[04]: https://github.com/faizann24/Using-machine-learning-to-detect-malicious-URLs
