#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""helpers.py: helper functions for project"""

import time

import pandas as pd

import app.tokenizer as atk


def select_rows_with_token(token: str, df: pd.DataFrame):
    data = list()
    for index, row in df.iterrows():
        url = row['url']
        if token in atk.split_expression(url):
            data.append([url, row['label']])
    return pd.DataFrame(data, columns=['url', 'label'])


def get_statistics(word: str, df: pd.DataFrame):
    word = word.lower()
    selection = select_rows_with_token(word, df)
    stats = selection.groupby(['label']).agg(['count'])
    return stats


def timeit(method):
    """Timing decorator

    Source: https://medium.com/pythonhive/python-decorator-to-measure-the-execution-time-of-methods-fa04cb6bb36d

    Example
    -------
    >>> @timeit
    >>> def foo(**kwargs):
    >>>     print 'foo'

    >>> logtime_data = {}
    >>> bar = foo(log_time=logtime_data)

    :param method: name of the timed function
    :return:
    """

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % (method.__name__, (te - ts) * 1000))
        return result

    return timed


def __main():
    pass


if __name__ == '__main__':
    __main()
