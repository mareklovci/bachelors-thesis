#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""__main__.py: main file containing classifier"""

__author__ = "Marek Lovci"

import matplotlib.pyplot as plt
import numpy as np
from lime.lime_text import LimeTextExplainer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

from app.datasets import get_dataframe, Datasets, test_urls
from app.helpers import timeit
from app.tokenizer import get_tokens

class_names = ['good', 'bad']


@timeit
def process(dataset=Datasets.five2five.value):
    df = get_dataframe(dataset)  # read file

    df = np.array(df)  # converting it into an array
    np.random.shuffle(df)  # shuffling
    # df = df[:5000]  # temporary limit no of items -> faster

    y = [d[1] for d in df]  # all labels
    corpus = [d[0] for d in df]  # all urls corresponding to a label (either good or bad)
    vectorizer = TfidfVectorizer(tokenizer=get_tokens)  # get a vector for each url but use our customized tokenizer
    x = vectorizer.fit_transform(corpus)  # get the X vector

    # split into training and testing set (e.g. 80/20 ratio)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0, random_state=42)

    clf = SGDClassifier(alpha=0.0001,  # better score with 0.00001
                        average=False,
                        class_weight=None,
                        epsilon=0.1,
                        eta0=0.0,
                        fit_intercept=True,
                        l1_ratio=0.15,
                        learning_rate='optimal',
                        loss='log',  # cannot be 'hinge' because of Lime explainer (not supported)
                        max_iter=5,
                        n_iter=None,
                        n_jobs=-1,
                        penalty='l2',
                        power_t=0.5,
                        random_state=42,
                        shuffle=True,
                        tol=None,
                        verbose=0,
                        warm_start=False
                        )

    clf.fit(x_train, y_train)
    return vectorizer, clf


def explain(vectorizer, clf, url, true_class):
    pipeline = Pipeline(memory=None, steps=[('anova', vectorizer), ('svc', clf)])
    explainer = LimeTextExplainer(class_names=class_names)
    exp = explainer.explain_instance(url, pipeline.predict_proba, num_features=6)  # explanation

    print('Probability(good) =', pipeline.predict_proba([url])[0, 1])
    print('True class: {}'.format(true_class))

    exp.as_list()

    exp.as_pyplot_figure()
    plt.show()


def __main():
    vectorizer, clf = process()

    # checking some random URLs
    (x, y) = zip(*test_urls.items())

    # explain with Lime
    explain(vectorizer, clf, x[2], y[2])

    # show statistics
    x_predict = vectorizer.transform(x)
    y_predict = clf.predict(x_predict)
    report = classification_report(y, y_predict, target_names=list(np.unique(y)))
    print(report)


if __name__ == '__main__':
    __main()
