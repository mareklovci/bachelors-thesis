#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""tokenizer.py: file with tokenizer(s) for main classifier"""

from typing import Iterable

import wordninja

__words: list = ['thequickbrownfoxjumpsoverthelazydog',
                 'llanfairpwllgwyngyllgogerychwyrndrobwll-llantysiliogogogoch.co',
                 'https://www.degruyter.com/assets/virtual/H4sIAAAAAAAAAKWSwUo',
                 'spokeo.com/Norman+Riley',
                 'hotelplanner.com/zip/Melville-NY-hotels-in-11747',
                 'facebook.com/pages/Concordia-Stingers/21636242760',
                 'pakistanifacebookforever.com/getpassword.php/',
                 'www.radsport-voggel.de/wp-admin/includes/log.exe',
                 'www.itidea.it/centroesteticosothys/img/_notes/gum.exe'
                 ]

# removing .com, it occurs a lot of times and it shouldn't be included in our features
unwanted_words = ('com', 'www', 'http', 'https')
class_names = ('bad', 'good')


def get_tokens(url: str) -> list:
    """Tokenize string"""
    tokens = wordninja.split(url)
    lower_cased = [x.lower() for x in tokens]
    return lower_cased


def split_expression(url: str, stop_words: Iterable = unwanted_words) -> list:
    """Tokenize string"""
    all_tokens = get_tokens(url)
    tokens = [x for x in all_tokens if x not in stop_words]  # remove elements
    return tokens


def __main():
    for word in __words:
        print(word)
        print('get_tokens: {}'.format(get_tokens(word)))
        print('')


if __name__ == '__main__':
    __main()
