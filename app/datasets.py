#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""datasets.py: Module with functions used for serving data to classifier"""

import os
from enum import Enum
from os.path import join
from pathlib import Path

import pandas as pd

# initialize path to data folder
data_folder = os.path.normpath(os.path.abspath('') + os.sep + os.pardir + '/data/')

_available_datasets = ['data_10_90.csv', 'data_20_80.csv', 'data_30_70.csv', 'data_40_60.csv', 'data_50_50.csv',
                       'data_60_40.csv', 'data_70_30.csv', 'data_80_20.csv', 'data_90_10.csv']

test_urls = {'spokeo.com/Norman+Riley': 'good',
             'hotelplanner.com/zip/Melville-NY-hotels-in-11747': 'good',
             'facebook.com/pages/Concordia-Stingers/21636242760': 'good',
             'pakistanifacebookforever.com/getpassword.php/': 'bad',
             'www.radsport-voggel.de/wp-admin/includes/log.exe': 'bad',
             'www.itidea.it/centroesteticosothys/img/_notes/gum.exe': 'bad'
             }


class Datasets(Enum):
    """Enumeration containing available datasets

    Artificially made files with url adresses in certain ratio **bad**/**good**

    Example
    -------

    >>> print(Datasets.one2nine.value)
    """
    one2nine = _available_datasets[0]
    two2eight = _available_datasets[1]
    three2seven = _available_datasets[2]
    four2six = _available_datasets[3]
    five2five = _available_datasets[4]
    six2four = _available_datasets[5]
    seven2three = _available_datasets[6]
    eight2two = _available_datasets[7]
    nine2one = _available_datasets[8]


# initialize path to dataset
def get_path(data: str = Datasets.five2five.value):
    return Path(join(data_folder, 'partials', data))


def get_dataframe(dataset: str = Datasets.five2five.value):
    """Return pandas dataframe for given dataset parameter

    :param dataset: defaults to 'data_50_50.csv'
    :return: pandas dataframe
    """
    path = Path(join(data_folder, 'partials', dataset))
    return pd.read_csv(path, ',', error_bad_lines=False)


def __main():
    pass


if __name__ == '__main__':
    __main()
