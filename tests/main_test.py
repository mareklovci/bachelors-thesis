import unittest
from app import __main__


class TestNameGenerator(unittest.TestCase):

    def setUp(self):
        self.main = __main__
        self.expected = ['Day0.svm', 'Day1.svm', 'Day2.svm', 'Day3.svm', 'Day4.svm']
        self.result = list(self.main.generate_names(0, 5))

    def test_count_eq(self):
        """Tests same items in lists"""
        self.assertCountEqual(self.result, self.expected)

    def test_list_eq(self):
        """Tests same items and order in list"""
        self.assertListEqual(self.result, self.expected)


if __name__ == '__main__':
    unittest.main()
