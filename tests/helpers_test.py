import unittest
import app.helpers as ahp
import app.datasets as ads
import pandas as pd


class TestHelpers(unittest.TestCase):

    def setUp(self):
        self.dataframe = pd.read_csv(ads.get_path(), ',', error_bad_lines=False)

    def test_list_eq(self):
        self.assertIsNotNone(ahp.select_rows_with_token('facebook', self.dataframe))


if __name__ == '__main__':
    unittest.main()
