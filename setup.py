from setuptools import setup

setup(name='thesis',
      version='0.1.0',
      packages=['app'],
      entry_points={
          'console_scripts': [
              'thesis = app.__main__:__main'
          ]
      }, install_requires=['matplotlib', 'numpy', 'lime', 'scikit-learn', 'pandas', 'wordninja']
      )
